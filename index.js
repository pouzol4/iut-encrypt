"use strict";

const crypto = require('crypto');
const KEY = "AHk7_hKmER(5"

exports.sha1 = function (password) {
    return crypto.createHmac('sha1', KEY)
        .update(password, 'binary')
        .digest('hex');
}

exports.compareSha1 = function (password, passwordCrypt) {
    return this.sha1(password) === passwordCrypt;
}